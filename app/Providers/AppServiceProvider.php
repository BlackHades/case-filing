<?php

namespace App\Providers;

use App\Interfaces\CaseInterface;
use App\Interfaces\UserInterface;
use App\Repositories\CaseRepository;
use App\Repositories\UserRepositories;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(UserInterface::class,UserRepositories::class);
        $this->app->singleton(CaseInterface::class,CaseRepository::class);
    }
}
