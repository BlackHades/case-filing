<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;


class User extends Authenticatable
{

    protected $fillable = ["name","address","password","email","phone_number"];

    public function cases(){
        return $this->hasMany(CourtCase::class,'user_id','id');
    }
}
