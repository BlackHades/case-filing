<?php

namespace App\Http\Controllers;

use App\Helper\AppMailer;
use App\models\User;
use App\Repositories\UserRepositories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Barryvdh\Debugbar\Facade as Debugbar;


class AccountController extends Controller
{
    //

    protected $users;

    public function __construct(UserRepositories $userRepositories)
    {
        $this->users = $userRepositories;
    }


    //GET
    public function register()
    {
        return view('utility.register',['title' => 'Register']);
    }

    public function login()
    {
        Auth::logout();
        return view('utility.default',['title' => 'Login']);
    }


    //Post
    public function registerPost(Request $request)
    {
        //$this->users->truncate();
        $this->validate($request,
            [
                'name' => 'required',
                'email' => 'required|unique:users,email',
                'address' => 'required',
                'phone_number' => 'required',
                'password' => 'required',
                'confirm_password' => 'required|same:password'
            ],[
                'confirm_password.same' => '  Passwords Does Not Match',
                'phone_number.required' => '  Phone Number is Required',
            ]
        );
        $u = new User();
        $u->name = $request->name;
        $u->password = bcrypt($request->password);
        $u->address = $request->address;
        $u->phone_number = $request->phone_number;
        $u->email = $request->email;
        $user = $this->users->save($u);
        if(isset($user)){
            Log::info('user', [$user->id]);
            Auth::loginUsingId($user->id);
            if(Auth::user()->role == 1)
            {
                return redirect()->action('AdminController@dashboard');
            }
            elseif(Auth::user()->role == 2)
            {
                return redirect()->action('UserController@dashboard');
            }
            else{
                Session::flash('error', 'No Role Defined. Contact Administrator.');
                Auth::logout();
                return redirect()->back();
            }
        }else{
            Session::flash('error', 'An Error Occurred when creating user. Please try again.');
            return redirect()->back();
        }
    }

    public function loginPost(Request $request)
    {
        $this->validate($request,[
            'email' => 'required',
            'password' => 'required'
        ]);
        //dd($request->all());
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password]))
        {
            //dd(Auth::user());
            if(Auth::user()->role == 1)
            {
                return redirect()->action('AdminController@dashboard');
            }
            elseif(Auth::user()->role == 2)
            {
                return redirect()->action('UserController@dashboard');
            }
            else{
                Session::flash('error', 'No Role Defined. Contact Administrator.');
                Auth::logout();
                return redirect()->back();
            }
        }
        else
        {
            Session::flash('error','Incorrect Username/Password');
            return redirect()->back();
        }
    }

    public function logout()
    {
        if(Auth::Check())
        {
            Auth::logout();
            return redirect()->action('AccountController@login');

        }
        else{
            return redirect()->action('AccountController@login');
        }
    }

    public function forgotPassword()
    {
        return view('Account.forget_password',['title' => 'Forgot Password','t' => 1]);
    }

    public function forgotPasswordPost(Request $request, AppMailer $mailer)
    {
        $this->validate($request,[
            'email' => 'required'
        ]);
        //dd($request->all());
        return redirect()->back();

    }

    public function resetLink($token)
    {
        $d = ResetPassword::FindByToken($token);
        if($d == null)
        {
            Session::flash('error','Incorrect Token, Please Try Resetting Your Password Again');
            return view('Account.forget_password',['title' => 'Reset Password','t' => 3]);
        }
        else{
            return view('Account.forget_password',['title' => 'Reset Password','t' => 2, 'token' =>  $token]);
        }
    }

    public function recoverPassword(Request $request, $token)
    {
        $this->validate($request,[
            'email' =>  'required',
            'new_password' => 'required',
            'conf_new_password' => 'required|same:new_password',
        ],['conf_new_password' => 'Password Mismatch']);

        $d = ResetPassword::FindByToken($token);
        if($d == null)
        {
            return redirect()->action('AccountController@ForgotPassword');
        }

        if($request->email === $d->email || $token === $d->token)
        {
            $m = User::FindByEmail($d->email);
            if($m != null)
            {
                $m->password = Hash::make($request->new_password);
                $m->save();
                $d->used = true;
                $d->save();
                Session::flash('success','Password Successfully Changed');
                return redirect()->action('AccountController@Login');
            }
            else{
                Session::flash('error','Sorry, We Could Not Find The Account You Provided');
                return redirect()->action('AccountController@Register');
            }
        }
    }

    public function test(){

    }
}
