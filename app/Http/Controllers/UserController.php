<?php

namespace App\Http\Controllers;

use App\models\CourtCase;
use App\Repositories\CaseRepository;
use App\Repositories\UserRepositories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Barryvdh\Debugbar\Facade as Debugbar;

class UserController extends Controller
{
    //
    protected $users;
    protected $cases;

    public function __construct(UserRepositories $userRepositories, CaseRepository $caseRepository)
    {
        $this->users = $userRepositories;
        $this->cases = $caseRepository;
    }

    function dashboard(){
        return view('users.dashboard',['title' => 'Dashboard', 'case' =>  $this->cases->userCase(Auth::id())]);
    }

    function caseAdd(){
        return view('users.case_add', ['title' => 'Add Case']);
    }

    function casePost(Request $request){
        $this->validate($request,[
            'plaintiff' => 'required',
            'defendant' => 'required',
        ]);
        $c = new CourtCase();
        $c->plaintiff = $request->plaintiff;
        $c->user_id = Auth::id();
        $c->defendant = $request->defendant;
        $c->approved = 0;
        $c->caption = $request->caption;
        $case = $this->cases->save($c);
        if(isset($case)){
            Session::flash("success", "Case Successfully filed");
        }else{
            Session::flash('error', 'An Error Occurred when creating case. Please try again.');
        }
        return redirect()->back();

    }
    function caseDelete(Request $request){
        if($request->has('id')){
            if($this->cases->delete($request->id))
                Session::flash('success','Case Deleted Successfully');
            else
                Session::flash('error','Unable to delete case now.');
        }else{
            Session::flash('error', 'An error occurred, please try again');
        }
        return redirect()->back();
    }
}
