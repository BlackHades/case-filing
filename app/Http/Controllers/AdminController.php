<?php

namespace App\Http\Controllers;

use App\Repositories\CaseRepository;
use App\Repositories\UserRepositories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Barryvdh\Debugbar\Facade as Debugbar;

class AdminController extends Controller
{
    protected $users;
    protected $cases;

    public function __construct(UserRepositories $userRepositories, CaseRepository $caseRepository)
    {
        $this->users = $userRepositories;
        $this->cases = $caseRepository;
    }

    public function dashboard(){
        return view('admin.dashboard',['title' => "Dashboard", "case" => $this->cases->all()]);
    }

    function caseAction(Request $request){
        debugbar()->info($request->all());
        Debugbar::info($request->all());
        if($request->has('action') && $request->has('id')){
            if($request->action == "decline"){
                $res = $this->cases->changeApproval($request->id, 2);
                if(isset($res)){
                    Session::flash('success','case Has been declined.');
                }else{
                    Session::flash('error','operation could not be completed');
                }
                return redirect()->back();
            }

            if($request->action == "approve"){
                $case = $this->cases->find($request->id);
                return view('admin.approved_case',['title' => 'Case Approval', 'case' => $case]);
            }
        }else{
            Session::flash('error','An Error occurred');
            return redirect()->back();
        }
    }

    function caseUpdate(Request $request){

        \debugbar()->info($request->all());
        \debugbar()->info($request);
        $this->validate($request,[
            'court_date' => 'required',
            'case_number' => 'required'
        ]);
        $case = $this->cases->find($request->id);
        $case->court_date = $request->court_date;
        $case->case_number = $request->case_number;
        $case->approved = 1;
        $cs = $this->cases->save($case);
        if(isset($cs)){
            Session::flash('success','Case Has Been Approved');
            return redirect()->action('AdminController@dashboard');
        }else{
            Session::flash('error','Unable to update case');
            return redirect()->back();
        }

    }
    function users(){
        return view('admin.users',['title' => "Users", 'user' => $this->users->all()]);
    }
}

