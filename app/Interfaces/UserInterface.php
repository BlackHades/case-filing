<?php
/**
 * Created by PhpStorm.
 * User: robot
 * Date: 5/20/18
 * Time: 7:33 PM
 */

namespace App\Interfaces;


use App\models\User;

interface UserInterface
{
    public function all();
    public function find($id);
    public function save(User $user);
}