<?php
/**
 * Created by PhpStorm.
 * User: robot
 * Date: 5/21/18
 * Time: 12:08 AM
 */

namespace App\Interfaces;


use App\models\CourtCase;

interface CaseInterface
{
    public function all();
    public function find($id);
    public function save(CourtCase $case);
    public function delete($id);
    public function changeApproval($id, $value);
    public function userCase($id);
}