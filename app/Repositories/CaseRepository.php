<?php
/**
 * Created by PhpStorm.
 * User: robot
 * Date: 5/21/18
 * Time: 12:11 AM
 */

namespace App\Repositories;


use App\Interfaces\CaseInterface;
use App\models\CourtCase;
use Illuminate\Support\Facades\Log;
use PHPUnit\Framework\Constraint\CountTest;

class CaseRepository implements CaseInterface
{
    public function all()
    {
        return CourtCase::all();
    }

    function userCase($id){
        return CourtCase::where('user_id', $id)->get();
    }

    public function find($id)
    {
        return CourtCase::find($id);
    }

    public function save(CourtCase $user){
        try{
            if($user->save())
                return $user;
            else
                return null;
        }catch (\Exception $ex){
            Log::info('user', [$user]);
            Log::info('user', [$ex]);
            return null;
        }
    }

    public function truncate()
    {
        CourtCase::truncate();
    }

    function delete($id){
        return CourtCase::find($id)->delete();
    }

    function changeApproval($id, $value)
    {
        $d = CourtCase::find($id);
        $d->approved = $value;
        if ($d->save())
            return $d;
        else
            return null;
    }
}