<?php
/**
 * Created by PhpStorm.
 * User: robot
 * Date: 5/20/18
 * Time: 7:31 PM
 */

namespace App\Repositories;


use App\Interfaces\UserInterface;
use App\models\User;
use Illuminate\Support\Facades\Log;

class UserRepositories implements UserInterface
{

    public function all()
    {
        return User::all();
    }

    public function find($id)
    {
        return User::find($id);
    }

    public function save(User $user){
        try{
            if($user->save())
                return $user;
            else
                return null;
        }catch (\Exception $ex){
            Log::info('user', [$user]);
            Log::info('user', [$ex]);
            return null;
        }
    }

    public function truncate()
    {
        User::truncate();
    }
}