<?php

use Illuminate\Support\Facades\Artisan;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Utility
Route::get('/clear', function ()
{
    Artisan::call('view:clear');
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    dd('cleared');
});

//Home
Route::get('/','AccountController@login')->name('home');
Route::get('/about-and-faq','HomeController@About')->name('about');
Route::get('/contact','HomeController@Contact')->name('contact');

//Account
Route::get('/register','AccountController@register')->name('register');
Route::get('/register/referral/{r_link}','AccountController@registerRef')->name('register_referrals');
Route::post('/register','AccountController@registerPost')->name('register_post');
Route::get('/login','AccountController@login')->name('login');
Route::post('/login','AccountController@loginPost')->name('login_post');
Route::get('/logout','AccountController@logout')->name('logout');
Route::get('/forgot-password','AccountController@forgotPassword')->name('forgot_password');
Route::post('/forgot-password/post','AccountController@forgotPasswordPost')->name('forgot_password_post');
Route::get('/reset-password/{token}','AccountController@resetLink')->name('reset_link');
Route::post('/reset-password/{token}','AccountController@recoverPassword')->name('change_password');
Route::post('/mail/visitor','UtilityController@SendVMail')->name('Vmail');


Route::group(['prefix' => '/user/','middleware' => ['auth']], function ()
{
    Route::get('dashboard','UserController@dashboard')->name('user_dashboard');
    Route::get('case/add','UserController@caseAdd')->name('case.add');
    Route::post('case/post',"UserController@casePost")->name('case.post');
    Route::get('case/delete',"UserController@caseDelete")->name('case.delete');
});

Route::group(['prefix' => '/admin/','middleware' => ['auth']], function ()
{
    Route::get('dashboard','AdminController@dashboard')->name('admin.dashboard');
    Route::get('case/action','AdminController@caseAction')->name('case.action');
    Route::post('case/update','AdminController@caseUpdate')->name('case.update');
    Route::get('users','AdminController@users')->name('admin.user');
});


Route::get('test',"AccountController@test");