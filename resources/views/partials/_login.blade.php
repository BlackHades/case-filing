@include('partials._navbar')
<div class="page-content d-flex align-items-stretch" style="background-color: white!important;">
    @if(\Illuminate\Support\Facades\Auth::check())
        @include('partials._sidebar')
    @endif
    @yield('body')
</div>