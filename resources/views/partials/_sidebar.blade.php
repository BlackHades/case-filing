<nav class="side-navbar">
    <!-- Sidebar Header-->
    <div class="sidebar-header d-flex align-items-center">
        <div class="title">
            <h1 class="h4">{{Auth::user()->name}}</h1>
        </div>
    </div>
    <ul class="list-unstyled">
        @if(\Illuminate\Support\Facades\Auth::user()->role == 1)
            <li class="{{Request::is('admin/dashboard') ? "active" : " "}}"> <a href="{{route('admin.dashboard')}}"><i class="fa fa-book"></i>&nbsp;Cases </a></li>
            <li class="{{Request::is('admin/user') ? "active" : " "}}"> <a href="{{route('admin.user')}}"><i class="fa fa-user"></i>&nbsp;Users </a></li>
        @elseif(\Illuminate\Support\Facades\Auth::user()->role == 2)
            <li class="{{Request::is('user/dashboard') ? "active" : " "}}"> <a href="{{route('user_dashboard')}}"><i class="fa fa-book"></i>Cases</a></li>
            <li class="{{Request::is('user/case/add') ? "active" : " "}}"> <a href="{{route('case.add')}}"><i class="fa fa-plus"></i>Add Cases</a></li>
        @endif
    </ul>
</nav>