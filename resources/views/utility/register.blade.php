@extends('master')
@section('body')
    <div class="container">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card center-block" style="margin-top: 150px">
                    <div class="card-header">
                        <header>Register Here...</header>
                        @include("partials._message")
                    </div>
                    <div class="card-body align-items-center">
                        <form action="{{route("register_post")}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="name">Full Name:</label>
                                <input type="text" class="form-control" placeholder="Enter Your Fullname here..." id="name" name="name" required/>
                            </div>
                            <div class="form-group">
                                <label for="email">Email Address:</label>
                                <input type="email" class="form-control" placeholder="Enter email address here..." id="email" name="email" required/>
                            </div>
                            <div class="form-group">
                                <label for="address">Residential Address:</label>
                                <textarea class="form-control" rows="3" placeholder="Enter residential address here..." id="address" name="address" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="phone_number">Phone Number:</label>
                                <input type="number" class="form-control" placeholder="Enter phone number here..." id="phone_number" name="phone_number" required/>
                            </div>
                            <div class="form-group">
                                <label for="password">Password:</label>
                                <input type="password" class="form-control" placeholder="Enter password here..." id="password" name="password" required/>
                            </div>
                            <div class="form-group">
                                <label for="confirm_password">Confirm Password:</label>
                                <input type="password" class="form-control" placeholder="Confirm password here..." id="confirm_password" name="confirm_password" required/>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary btn-block" type="submit">Sign In</button>
                            </div>
                        </form>
                        <p class="align-content-center" style="text-align: center!important;">
                            <a href="{{route("login")}}" class="btn btn-link" style="text-align: center!important;">Already have an account, Log in</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection