@extends('master')
@section('body')
    <div class="container">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card center-block" style="margin-top: 150px">
                    <div class="card-header">
                        <header>Sign In Here...</header>
                        @include("partials._message")
                    </div>
                    <div class="card-body align-items-center">
                        <form action="{{route("login_post")}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="email">Email Address:</label>
                                <input type="email" class="form-control" placeholder="Enter email address here..." id="email" name="email" required/>
                            </div>
                            <div class="form-group">
                                <label for="email">Password:</label>
                                <input type="password" class="form-control" placeholder="Enter password here..." id="password" name="password" required/>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary btn-block" type="submit">Sign In</button>
                            </div>
                        </form>
                        <p class="align-content-center" style="text-align: center!important;">
                            <a href="{{route("register")}}" class="btn btn-link" style="text-align: center!important;">Click here to register</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection