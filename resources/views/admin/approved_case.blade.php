@extends('master')
@section('body')
    <div class="content-inner">
        <!-- Page Header-->
        <header class="page-header">
            <div class="container-fluid">
                <h2 class="no-margin-bottom">Approve Case</h2>
            </div>
        </header>
        <div class="container">
            @include('partials._message')
            <div class="row">
                <div class="col-lg-6">
                    <form action="{{route("case.update")}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <input type="text" hidden class="form-control" value="{{$case->id}}" id="id" name="id" required/>
                        </div>
                        <div class="form-group">
                            <label for="name">Name of Plaintiff:</label>
                            <input type="text" disabled class="form-control" value="{{$case->plaintiff}}" placeholder="Name of plaintiff here..." id="plaintiff" name="plaintiff" required/>
                        </div>
                        <div class="form-group">
                            <label for="name">Name of Defendant:</label>
                            <input type="text" disabled class="form-control" value="{{$case->defendant}}" placeholder="Name of Defendant here..." id="defendant" name="defendant" required/>
                        </div>
                        <div class="form-group">
                            <label for="caption">Caption/Claim:</label>
                            <textarea rows="3" disabled class="form-control" id="caption" name="caption" placeholder="Enter Caption Or Claims Here...">{{$case->caption}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="courtdate">Court Date:</label>
                            <input type="text" class="form-control" placeholder="Court Date" id="court_date" name="court_date" required/>
                        </div>
                        <div class="form-group">
                            <label for="case_number">case Number:</label>
                            <input type="text" class="form-control" id="case_number" name="case_number" placeholder="Enter case Number..."/>
                        </div>
                        <p>Note: Your can't edit after submission.</p>
                        <div class="form-group">
                            <button class="btn btn-primary btn-block" type="submit">Submit Case</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        jQuery('#court_date').datetimepicker();
    </script>
@endsection