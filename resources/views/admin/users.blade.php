@extends('master')
@section('body')
    <div class="content-inner">
        <!-- Page Header-->
        <header class="page-header">
            <div class="container-fluid">
                <h2 class="no-margin-bottom">List of Cases</h2>
            </div>
        </header>
        <div class="container">
            @include('partials._message')
            <table class="table table-responsive">
                <thead>
                <th>S/N</th>
                <th>Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>Phone Number</th>
                <th>Role</th>
                </thead>
                <tbody>
                <?php $i = 1?>
                @foreach($user as $cs)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$cs->name}}</td>
                        <td>{{$cs->email}}</td>
                        <td>{{$cs->address}}</td>
                        <td>{{$cs->phone_number}}</td>
                        <td>{{$cs->role == 1 ? "Admin" : "User"}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection