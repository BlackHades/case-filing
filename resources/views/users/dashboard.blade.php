@extends('master')
@section('body')
    <div class="content-inner">
        <!-- Page Header-->
        <header class="page-header">
            <div class="container-fluid">
                <h2 class="no-margin-bottom">List of Cases</h2>
            </div>
        </header>
        <div class="container">
            @include('partials._message')
            <table class="table table-responsive">
                <thead>
                    <th>S/N</th>
                    <th>Submission Date</th>
                    <th>Plaintiff</th>
                    <th>Defendant</th>
                    <th>Caption</th>
                    <th>Case Number</th>
                    <th>Court Date</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    <?php $i = 1?>
                    @foreach($case as $cs)
                        <tr class="@if($cs->approved == 1)
                                alert alert-success
                                @elseif($cs->approved == 2)
                                 alert alert-danger
                                @else
                                    alert alert-warning
                                @endif
                        ">
                            <td>{{$i++}}</td>
                            <td>{{$cs->created_at}}</td>
                            <td>{{$cs->plaintiff}}</td>
                            <td>{{$cs->defendant}}</td>
                            <td>{{$cs->caption}}</td>
                            <td>{{$cs->court_date == null ? "N/A" : $cs->court_date}}</td>
                            <td>{{$cs->case_number == null ? "N/A" : $cs->case_number}}</td>
                            <td>
                                @if($cs->approved == 0)
                                    <a href="{{route('case.delete',['id' => $cs->id])}}" onclick="return confirm('Are you sure you want to delete this case? This process is irreversible')" class="btn btn-outline-danger btn-sm" style="margin-right: 10px;" data-toggle="tooltip" title="Delete Case"><i class="fa fa-trash"></i></a>
                                @else
                                    N/A
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection