@extends('master')
@section('body')
    <div class="content-inner">
        <!-- Page Header-->
        <header class="page-header">
            <div class="container-fluid">
                <h2 class="no-margin-bottom">File A Case</h2>
            </div>
        </header>
        <div class="container" style="margin: 50px">
            @include('partials._message')
            <div class="row">
               <div class="col-lg-6">
                   <form action="{{route("case.post")}}" method="post">
                       {{csrf_field()}}
                       <div class="form-group">
                           <label for="name">Name of Plaintiff:</label>
                           <input type="text" class="form-control" placeholder="Name of plaintiff here..." id="plaintiff" name="plaintiff" required/>
                       </div>
                       <div class="form-group">
                           <label for="name">Name of Defendant:</label>
                           <input type="text" class="form-control" placeholder="Name of Defendant here..." id="defendant" name="defendant" required/>
                       </div>
                       <div class="form-group">
                           <label for="caption">Caption/Claim:</label>
                           <textarea rows="3" class="form-control" id="caption" name="caption" placeholder="Enter Caption Or Claims Here..."></textarea>
                       </div>
                       <p>Note: Your can't edit after submission.</p>
                       <div class="form-group">
                           <button class="btn btn-primary btn-block" type="submit">Submit Case</button>
                       </div>
                   </form>
               </div>
           </div>

        </div>
    </div>
@endsection